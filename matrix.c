#include <stdio.h>
 
int main (){
 
  int n, i, j, is_uppr=1, is_lowr=1, a;
 
  printf(" ----CHECK IF A MATRIX IS TRIANGULAR OR NOT----\n\n");
  printf("Enter the number of rows:: ");
  scanf("%d",&n);
 
  printf("Enter the Array::\n");
  for( i=0; i<n; i++){
    for( j=0; j<n; j++){
      scanf("%d",&a);
      if( j>i && a!=0)//Check for upper triangular condition
    is_uppr = 0;
      if( j<i && a!=0)//Check for lower triangular condition
    is_lowr = 0;
    }
  }
  if( is_uppr==1 || is_lowr==1)
    printf("Yes, matrix is triangular...\n");
  else
    printf("No, matrix is not triangular...\n");
 
  return 0;
 
}